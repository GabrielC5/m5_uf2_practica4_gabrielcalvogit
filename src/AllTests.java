import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;
//Gabriel
@RunWith(Suite.class)
@SuiteClasses({ CalculadoraTestDivisio.class, CalculadoraTestMultiplicacio.class, CalculadoraTestResta.class,
		CalculadoraTestSuma.class, LaCalculadoraTest.class })
public class AllTests {

}
