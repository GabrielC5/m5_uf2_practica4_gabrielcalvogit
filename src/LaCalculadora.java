import java.util.Scanner;
// Gabriel
/**
 * <h2>clase Calculadora, se utiliza para calcular operaciones como suma, resta, divisi髇 y multiplicaci髇 </h2>
 * 
 * Busca informaci髇 de Javadoc en <a href="http://www.google.com">GOOGLE</a>
 * @see <a href="http://www.google.com">Google</a>
 * @version 5-2021
 * @author Gabriel
 * @since 07-05-2021
 */
public class LaCalculadora {

    static Scanner reader = new Scanner(System.in);

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		int op1 = 0, op2 = 0;
		/**
		 * atributo para escoger la opcion deseada
		 */
		char opcio;
		/**
		 * atributo para guardar el resultado de la operacion
		 */
		int resultat = 0;
		boolean control = false;

		do {
			mostrar_menu();
			opcio = llegirCar();

			switch (opcio) {
                case 'o':
                case 'O':
                    op1 = llegirEnter();
                    op2 = llegirEnter();
                    control = true;
                    break;
                case '+':
                    if (control)
                        resultat = suma(op1, op2);
                    else mostrarError();
                    break;
                case '-':
                    if (control)
                        resultat = resta(op1, op2);
                     else mostrarError();
                    break;
                case '*':
                    if (control)
                        resultat = multiplicacio(op1, op2);
                     else mostrarError();
                    break;
                case '/':
                    if (control) {
                    	if(op1==0 || op2 ==0) {
                    		
                    	}
                    	else {
                    		resultat = divisio(op1, op2);
                    	}
                        if (resultat == -99)
                            System.out.print("No ha fet la divisi贸");
                        else
                             visualitzar(resultat);
					}
		            else mostrarError();
                    break;
                case 'v':
                case 'V':
                    if (control)
                        visualitzar(resultat);
                     else mostrarError();
                    break;
                case 's':
                case 'S':
                    System.out.print("Acabem.....");

                    break;
                default:
                    System.out.print("opci贸 erronia");
			}
			;
		} while (opcio != 's' && opcio != 'S');
		System.out.print("\nAdeu!!!");

	}
	
	/**
	 * Muestra un mensaje de error
	 * @see LaCalculadora
	 */
    public static void mostrarError() { /* procediment */
		System.out.println("\nError, cal introduir primer els valors a operar");
	}
    //Gabriel

    /**
	 * Suma los dos valores introducidos por parametros
	 * @see LaCalculadora
	 * @param int valor a sumar y int segundo valor a sumar
	 * @return int resultado
	 */

	public static int suma(int a, int b) { /* funci贸 */
		int res;
		res = a + b;
		return res;
	}
	/**
	 * Resta los dos valores introducidos por parametros
	 * @see LaCalculadora
	 * @param int valor a restar y int segundo valor a reastar
	 * @return int resultado
	 */

	public static int resta(int a, int b) { /* funci贸 */
		int res;
		res = a - b;
		return res;
	}
	/**
	 * Multiplica los dos valores introducidos por parametros
	 * @see LaCalculadora
	 * @param int valor a multiplicar y int segundo valor a multiplicar
	 * @return int resultado
	 */

	public static int multiplicacio(int a, int b) { /* funci贸 */
		int res;
		res = a * b;
		return res;
	}
	/**
	 * Divide los dos valores introducidos por parametros
	 * @see LaCalculadora
	 * @param int valor a dividir y int dividendo
	 * @return int resultado
	 */

	public static int divisio(int a, int b) { /* funci贸 */
		int res = -99;
		char op;

		do {
			System.out.println("M. " + a + " mod " + b);
			System.out.println("D  " + a + " / " + b);
			op = llegirCar();
			if (op == 'M' || op == 'm'){
                if (b == 0) 
                    System.out.print("No es pot dividir entre 0\n");
                else
                    res = a % b;
			}

			else if (op == 'D' || op == 'd'){
                    if (b == 0)
                        System.out.print("No es pot dividir entre 0\n");
                    else
                        res = a / b;
                    }
                else
                    System.out.print("opci贸 incorrecte\n");
		} while (op != 'M' && op != 'm' && op != 'D' && op != 'd');

		return res;
	}
	/**
	 * Lee por pantalla un car醕ter
	 * @see LaCalculadora
	 * @return char car醕ter introducido
	 */

	public static char llegirCar() // funci贸
	{
		char car;

		System.out.print("Introdueix un car谩cter: ");
		car = reader.next().charAt(0);

		//reader.nextLine();
		return car;
	}
	/**
	 * Lee por pantalla un entero
	 * @see LaCalculadora
	 * @return int valor introducido
	 */

	public static int llegirEnter() // funci贸
	{
		int valor = 0;
		boolean valid = false;

		do {
			try {
				System.out.print("Introdueix un valor enter: ");
				valor = reader.nextInt();
				valid = true;
			} catch (Exception e) {
				System.out.print("Error, s'espera un valor enter");
				reader.nextLine();
			}
		} while (!valid);

		return valor;
	}
	/**
	 * Imprime por pantalla el resulato
	 * @see LaCalculadora
	 * @param int valor del resultado
	 */

	public static void visualitzar(int res) { /* procediment */
		System.out.println("\nEl resultat de l'operacio 茅s " + res);
	}
	/**
	 * Imprime las opciones del menu
	 * @see LaCalculadora
	 */
	public static void mostrar_menu() {
		System.out.println("\nCalculadora:\n");
		System.out.println("o.- Obtenir els valors");
		System.out.println("+.- Sumar");
		System.out.println("-.- Restar");
		System.out.println("*.- Multiplicar");
		System.out.println("/.- Dividir");
		System.out.println("v.- Visualitzar resultat");
		System.out.println("s.- Sortir");
		System.out.print("\n\nTria una opci贸 i ");
	}
	//Gabriel
	/**
	 * Divide dos valores si el dividendo no es cero, si es 0 lanza una excepcion
	 * @see LaCalculadora
	 * @param int valor a dividir y int dividendo
	 * @return int resultado
	 */
	public static  int divideix(int num1, int num2) {
		int res;
	if (num2 == 0)
		throw new java.lang.ArithmeticException("Divisi� entre zero");
	else 
	res = num1/num2;
		return res;
	}

}